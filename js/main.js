var controller;
if (Modernizr.touch) {
  var myScroll;
  $(document).ready(function () {
    // wrap for isroll
    $("#content-wrapper")
      .addClass("scrollContainer")
      .wrapInner('<div class="scrollContent"></div>');

    // add iScroll
    myScroll = new IScroll('#content-wrapper', {scrollX: false, scrollY: true, scrollbars: true, useTransform: false, useTransition: false, probeType: 3});
  
    // update container on scroll
  myScroll.on("scroll", function () {
    controller.update();
  });

  // overwrite scroll position calculation to use child's offset instead of parents scrollTop();
  controller.scrollPos(function () {
    return -myScroll.y;
  });

  // refresh height, so all is included.
  setTimeout(function () {
      myScroll.refresh();
  }, 0);

    $("#content-wrapper").on("touchend", "a", function (e) {
      // a bit dirty workaround for links not working in iscroll for some reason...
      e.preventDefault();
      window.location.href = $(this).attr("href");
    });

  // manual set hight (so height 100% is available within scroll container)
    $(document).on("orientationchange", function () {
      $("section")
        .css("min-height", $(window).height())
        .parent(".scrollmagic-pin-spacer").css("min-height", $(window).height());
    });
    $(document).trigger("orientationchange"); // trigger to init
  });
  // init the controller
  controller = new ScrollMagic({
    container: "#content-wrapper",
    globalSceneOptions: {
      triggerHook: "onLeave"
    }
  });
} else {
  // init the controller
  controller = new ScrollMagic({
    globalSceneOptions: {
      triggerHook: "onLeave"
    }
  });
}

$(document).ready(function() {
  // Количество частей выставки
  var partsCount = 3;
  var partPrefix = "navigation-part";
  var partLinkPrefix = "to_part"

  window.showNavBar = function (num, options) {
    options = options || {};

    for (i = 1; i <= partsCount; i++) {
      if (i == num) {
        $("#" + partPrefix + num).show();
      } else {
        $("#" + partPrefix + i).hide();
      }
    }
  };

  window.setActiveNav = function(nav, part) {
    $("#" + partPrefix + nav + " li").removeClass("active");
    $("#" + partPrefix + nav + " #" + partLinkPrefix + nav + "_" + part).addClass("active");
  }

  $("#navigation-bar li").click(function(e) {
    var this_id = $(e.currentTarget).attr("id");
    $("#" + this_id + " a").trigger("click");
  });
});


$(function() {
  $('a[href*=#]:not([href=#])').click(function(e) {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      var scrollTime = parseInt(Math.abs(target.offset().top - $(document).scrollTop()) / 10 * 3);
      if (scrollTime < 2000) {
        scrollTime = 2000;
      }
      // console.log(scrollTime);
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, scrollTime);
        return false;
      }
    }
  });
});

$(document).ready(function() {
  loaderTexts = new Array("Загружаю выставку", "Загрузка может занять продолжительное время", "Пожалуйста, подождите");
  textsCount = textNumber =loaderTexts.length;

  changeLoaderText = function() {
    if ($("#loader-screen").css("display") === "block") {
      textNumber++;
      newText = loaderTexts[textNumber % textsCount]
      $("#loader-text").fadeOut(500, function() {
        $("#loader-text").text(newText).fadeIn(500);
      });
      setTimeout(changeLoaderText, 3000);
    }
  };
  setTimeout(changeLoaderText, 3000);

  $(window).load(function() {
    $("#loader-screen").fadeOut(500);
  });
  
});